<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
</head>
<body>
<h1>Buat Account Baru</h1>
<h2> Sign Up Form</h2>

<form action="/welcome" method="POST">
    @csrf
    <label for="fname">First name:</label><br><br>
    <input type="text" id="fname" name="fname" ><br><br>
    <label for="lname">Last name:</label><br><br>
    <input type="text" id="lname" name="lname" ><br><br>

    <label for="gender">Gender:</label><br><br>
    <input type="radio" id="male" name="gender" value="male">
    <label for="male">Male</label><br>
    <input type="radio" id="female" name="gender" value="female">
    <label for="female">Female</label><br>
    <input type="radio" id="other" name="gender" value="other">
    <label for="other">Other</label><br><br>

    <label>Nationality</label><br><br>
    <select name="nationality" id="">
        <option value="indonesian">Indonesian</option>
        <option value="singapore">Singaporean</option>
        <option value="malaysian">Malaysian</option>
        <option value="australian">Australian</option>
    </select><br><br>

    <label>Language Spoken:</label><br><br>
    <input type="checkbox" name="ls" value="bahasa indonesia">Bahasa Indonesia<br>
    <input type="checkbox" name="ls" value="english">English<br>
    <input type="checkbox" name="ls" value="other">Other<br><br>

    <label>Bio:</label><br><br>

    <textarea id="bio" name="Bio" rows="15" cols="30"></textarea><br>




   <input type="submit" value="Sign Up">

</form>
    
</body>
</html>